Eating in Fantasy (single page web application)                              

•	Developed a single page application for a restaurant with AngularJS, Bootstrap and JavaScript

•	Used Angular modules and controllers to organize the Angular application and separate the data from the layout of the page

•	Used task runners like Grunt and Gulp to automate the web development tasks

•	Performed form validation in the controller code with Angular's support for forms. Established two-way data binding to show a real-time preview of the comment on the page

•	Used Angular UI-Router to support multiple views for the single page application
